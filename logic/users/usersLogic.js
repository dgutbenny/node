var db = require('../../middleware/mysqlPool');
var datetime = require('silly-datetime');
var response = require('../common/response');

function addUser(res, data) {
    var create_time = datetime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
    filter = {
        'name': data.name,
        'create_time': create_time
    }
    db.insertData('users', filter, (error, result) => {
        if (error) {
            return response.error(res, 1000, error);
        }
        return response.success(res, {id: result.insertId});
    });
}

module.exports = {
    addUser: addUser
};
