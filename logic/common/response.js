function responseDoSuccess(res, data) {
    if (data == null) {
        data = {};
    }
    return res.status(200).json({
        "code": 0,
        "msg": 'success',
        'data': data
    });
}

function responseDoError(res, code, msg) {
    if (code == null) {
        code = 10000;
    }
    if (msg == null) {
        msg = 'error';
    }
    return res.status(200).json({
        "code": code,
        "msg": msg,
        'data': {}
    });
}
exports.success = responseDoSuccess;
exports.error = responseDoError;