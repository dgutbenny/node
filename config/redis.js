// 开发环境redis配置
let dev_redis = {
    host: "xxxxx",  //地址
    password: "xxxxx",   //密码
    port: 6379,             //端口
    pool_max_num : 10,      //redis最多连接数
    pool_min_num : 2        //redis最少连接数
};
// 生产环境redis配置
let pro_redis = {};

let redis = process.env.NODE_ENV === 'pro' ? pro_redis : dev_redis;

module.exports = redis;