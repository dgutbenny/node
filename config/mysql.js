/**
 * 配置mysql
 * @type {{password: string, database: string, port: number, host: string, multipleStatements: boolean, user: string}}
 */
let dev_mysql = {
    host: "xxxxxx",      //数据库的地址
    user: "xxxx",               //需要用户的名字
    password: "xxxx",   //用户密码 ，如果你没有密码，直接双引号就是
    database: "xxxx",           //数据库名字
    port: 3306,                //端口
    multipleStatements: true,   //MySql可执行多条sql语句
    acquireTimeout : 10000,     //获取连接时的超时配置 默认10000
    waitForConnection : true,   //在连接池的所有连接没有可用的时候，如果 是true 就让申请连接的排队等待 ，如果false 则返回一个错误，默认 true
    connectionLimit : 20,       //一次性建立的最大连接数目  默认为 10
    queueLimit: 0,              //连接池的最大排队数目 超出报错 如果为0，则没有限制数目，默认为0
};
let pro_mysql = {};
let mysql = process.env.NODE_ENV === 'pro' ? pro_mysql : dev_mysql;
module.exports = mysql;
