let log = {
    "appenders": {
        "access": {
            "type": "dateFile",
            "filename": "runtime/access.log",
            "pattern": "-yyyy-MM-dd"
        },
        "rule-console": {
            "type": "console"
        },
        "rule-file": {
            "type": "dateFile",
            "filename": "runtime/server-",
            "encoding": "utf-8",
            "maxLogSize": 10000000,
            "numBackups": 3,
            "pattern": "yyyy-MM-dd.log",
            "alwaysIncludePattern": true
        },

        "rule-error": {
            "type": "dateFile",
            "filename": "runtime/error-",
            "encoding": "utf-8",
            "maxLogSize": 1000000,
            "numBackups": 3,
            "pattern": "yyyy-MM-dd.log",
            "alwaysIncludePattern": true
        }
    },

    "categories": {
        "default": {
            "appenders": [
                "rule-console",
                "rule-file",
                "rule-error"
            ],
            "level": "debug"
        },
        "http": {
            "appenders": [
                "access"
            ],
            "level": "info"
        }
    }
}


module.exports = {
    log: log
}