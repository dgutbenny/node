var express = require('express');
var datetime = require('silly-datetime');

var router = express.Router();


var db = require('../middleware/mysqlPool');
var redis = require('../middleware/redisPool');
var response = require('../logic/common/response');


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/user/getUserInfo', function (req, res, next) {
    console.log('get用户请求数据为：');
    console.log(req.query);
    res.json({
        meta: {
            code: 200
        },
        data: {
            message: '蛤蟆皮'
        }
    });
});

router.post('/test/insert', function (req, res, next) {
    let param = req.body;
    if (param.name == null) {
        return response.error(res, 10000, '参数不存在');
    }
    var create_time = datetime.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
    var filter = {
        'name': param.name,
        'create_time': create_time
    };
    db.insertData('users', filter, (error, result) => {
        if (error) {
            return response.error(res, 10001, error);
        }
        return response.success(res, {
            id: result.insertId
        });
    })
});

router.post('/test/delete', function (req, res, next) {
    let id = req.body.id;
    if (id == null) {
        return response.error(res, 10000, '参数不存在');
    }
    var filter = {
        'id': id,
    };
    db.deleteData('users', filter, (error, result) => {
        if (error) {
            return response.error(res, 10001, error);
        }
        return response.success(res, {});
    })
});

router.post('/test/update', function (req, res, next) {
    let id = req.body.id;
    if (id == null) {
        return response.error(res, 10000, '参数不存在');
    }
    var filter = {
        'name': '888',
    };
    var where = {
        'id': id,
    };
    db.updateData('users', filter, where, (error, result) => {
        console.log(result);
        if (error) {
            return response.error(res, 10001, error);
        }
        return response.success(res, {});
    })
});

router.post('/test/select', function (req, res, next) {
    db.selectAll('users', '*', (error, result) => {
        if (error) {
            return response.error(res, 10001, error);
        }
        return response.success(res, result);
    })
});

router.post('/test/selectPage', function (req, res, next) {
    var page = req.body.page;
    var page_size = req.body.page_size;
    db.selectPage('users', '*', page, page_size, function (err, results) {
        if (err) {
            return response.error(res, 1000, err);
        }
        var all_count = results[0][0]['COUNT(*)'];
        var all_page = parseInt(all_count) / 4;
        var page_str = all_page.toString();
        if (page_str.indexOf('.') > 0) {
            all_page = parseInt(page_str.split('.')[0]) + 1;
        }
        var list = results[1];
        return response.success(res, {curr_page: page, total_pages: all_page, total: all_count, list: list});
    });
});

router.post('/redis/add', function (req, res, next) {
    redis.set(1, 'customer_', '1234', '3600', function (err, results) {
        if (err) {
            return response.error(res, 1000, err);
        }
        console.log(results);
        return response.success(res, results);
    })
});

module.exports = router;
