var express = require('express');
var router = express.Router();

var usersLogic = require('../logic/users/usersLogic');
var response = require('../logic/common/response');

/**
 *  模块路由地址
 */
const users = {
    add: '/add',   //添加用户
    del: '/del',   //删除用户
    get: 'get'     //获取用户信息
};

router.post(users.add, function (req, res, next) {
    let request = req.body;
    if (request.name == null) {
        return response.error(res, 10000, '参数name不存在');
    }
    return usersLogic.addUser(res, request);
});

module.exports = router;
