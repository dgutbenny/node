# 基于expre封装node框架

## 目录结构

~~~
├─bin						项目入口
│	├─www.js					项目启动文件
├─config					配置文件目录
│	├─logs.js			        log4js日志配置
│	├─mysql.js					mysql数据库配置	
│	├─openApi.js				对外api接口配置	
│	├─redis.js					redis缓存队列配置
├─db					    数据库结构
├─logic					    项目逻辑层
│	├─common				    公共模块
│	├─users			            用户模块	
│	├─....					    其他模块
├─middleware				项目中间件
│	├─log				        日志
│	├─mysql			            mysql数据库	
│	├─redis					    redis缓存队列
├─node_modules				加载中间件模块
├─public					前端公共模块，js，css，images
├─routes					项目控制层（路由）
├─runtime					日志目录
├─views					    项目视图层
├─.gitignore				git忽略配置文件
├─app.js			        app.js文件
├─package.json				package.json文件
├─package-lock.json			package-lock.json文件
├─README.md				    README 文件
~~~