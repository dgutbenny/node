//mysql连接池配置文件
var mysql = require('mysql');
var $dbConfig = require('../config/mysql'); //mysql配置文件的路径
// 使用连接池，避免开太多的线程，提升性能
var pool = mysql.createPool($dbConfig);

/**
 * 封装query之sql带不占位符func
 */
function query(sql, callback) {
    pool.getConnection(function (err, connection) {
        connection.query(sql, function (err, rows) {
            callback(err, rows);
            //释放链接
            connection.release();
        });
    });
}

/**
 * 封装query之sql带占位符func
 */
function queryArgs(sql, args, callback) {
    pool.getConnection(function (err, connection) {
        connection.query(sql, args, function (err, rows) {
            callback(err, rows);
            //释放链接
            connection.release();
        });
    });
}

// 查询所有数据
let selectAll = (table, filter, callback) => {
    var sql = 'SELECT ' + filter + ' FROM ' + table;
    query(sql, callback);
}


// 分页查询数据
let selectPage = (table, filter, pages, page_size, callback) => {
    var start = (pages - 1) * page_size;
    //page是传过来的页数（page-1乘以10就是起始行）
    var sql = 'SELECT COUNT(*) FROM ' + table + ';SELECT * FROM ' + table + ' LIMIT ' + start + ',' + page_size;
    console.log(sql);
    //COUNT(*) 函数返回在给定的选择中被选的行数。  //检索记录行,从start以后10行
    query(sql, callback);
}

// 插入一条数据
let insertData = (table, datas, callback) => {
    var fields = '';
    var values = '';
    for (var k in datas) {
        fields += k + ',';
        values = values + "'" + datas[k] + "',"
    }
    fields = fields.slice(0, -1);
    values = values.slice(0, -1);
    console.log(fields, values);
    var sql = "INSERT INTO " + table + '(' + fields + ') VALUES(' + values + ')';
    query(sql, callback);
}

// 更新一条数据
let updateData = function (table, sets, where, callback) {
    var _SETS = '';
    var _WHERE = '';
    var keys = '';
    var values = '';
    for (var k in sets) {
        _SETS += k + "='" + sets[k] + "',";
    }
    _SETS = _SETS.slice(0, -1);
    for (var k2 in where) {
        //  _WHERE+=k2+"='"+where[k2]+"' AND ";
        _WHERE += k2 + "=" + where[k2];
    }
    // UPDATE user SET Password='321' WHERE UserId=12
    //update table set username='admin2',age='55'   where id="5";
    var sql = "UPDATE " + table + ' SET ' + _SETS + ' WHERE ' + _WHERE;
    console.log(sql);
    query(sql, callback);
}

// 删除一条数据
let deleteData = function (table, where, callback) {
    var _WHERE = '';
    for (var k2 in where) {
        //多个筛选条件使用  _WHERE+=k2+"='"+where[k2]+"' AND ";
        _WHERE += k2 + "=" + where[k2];
    }
    // DELETE  FROM user WHERE UserId=12  注意UserId的数据类型要和数据库一致
    var sql = "DELETE  FROM " + table + ' WHERE ' + _WHERE;
    query(sql, callback);
}


//exports
module.exports = {
    insertData: insertData,
    updateData: updateData,
    deleteData: deleteData,
    selectAll: selectAll,
    selectPage: selectPage,
}